
import defaultImg from "../images/default.png";


const emp = [
    {
        name: "Rahool M Wadam",
        email: "rahool.wadam@hurix.com",
        img: defaultImg,
        hobbies: ["Carrom", "8 Ball Pool"]
    },
    {
        name: "Rupesh Pawar",
        email: "rupesh.pawar@hurix.com",
        img: defaultImg,
        hobbies: ["Music"]
    },
	{
        name: "Rohit Tripathi",
        email: "rohit.tripathi@hurix.com",
        img: defaultImg,
        hobbies: ["Cricket, Music, Chess"]
    }
];

export default emp;
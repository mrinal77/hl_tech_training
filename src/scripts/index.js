import '../styles/index.scss';

import data from "./data";

//console.log('webpack starterkit');

$(document).ready(function() {
    let teamHTML = ``;
    data.forEach(emp => {
        teamHTML += `
            <li>
                <img src="${emp.img}">
                <div class="info">
                    <h2 class="name">${emp.name}</h2>
                    <a href="mailto:${emp.email}" class="email">${emp.email}</a>
                    <p>Hobbies: <strong>${emp.hobbies.join(", ")}</strong></p>
                </div>
            </li>
        `;
    })

    $('#team').html(teamHTML)
});